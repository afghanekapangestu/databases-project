@extends('dashboard')

@section('content-header')
      <h1>
        Lihat Nilai
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Lihat Nilai</li>
      </ol>
@endsection

@section('table')

@if ($message = Session::get('status'))
      <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
@endif


<table class="table table-bordered table-striped dataTable text-center" id="datatable">
        <thead>
              <th>#</th>
              <th>Ulangan Akhir Semester</th>
              <th>Ulangan Tengah Semester</th>
              <th>Ulangan Harian</th>
              <th>Total</th>
        </thead>
        <tbody>
          @foreach ($data as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->uas }}</td>
                <td>{{ $item->uh}}</td>
                <td>{{ $item->uts}}</td>
                <td>{{ $item->total}}</td>
            </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('footer')
    <p>Lihat nilai kamu disini ya :D</p>
@endsection

@section('scriptjs')
    <script>
        $(document).ready(function(){
            $("#datatable").DataTable();
        })
    </script>
@endsection
    