@extends('dashboard')

@section('content-header')
      <h1>
        Tambah Nilai
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Nilai</li>
      </ol>
@endsection

@section('header_button')
    <h3 class="box-title">Data Akun</h3>
    <button class="btn btn-success" id="btnModalTrigger">(+) Tambah Nilai</button>
@endsection

@section('table')
@if ($message = Session::get('status'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
@endif
<table class="table table-bordered table-striped dataTable text-center" id="datatable">
        <thead>
              <th>#</th>
              <th>Nama</th>
              <th>Kelas</th>
              <th>Mapel</th>
              <th>Total</th>
              <th>Action</th>
        </thead>
        <tbody>
          @foreach ($data as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->kelas}}&nbsp;{{ $item->id_prodi}}&nbsp;{{ $item->nomor_kelas}}</td>
                <td>{{ $item->nama_mapel}}</td>
                <td>{{ $item->total}}</td>
                <td>
                    <button class="btn btn-warning d-inline"  type="button" onclick="getNilaiByNis({{ $item->nis }});">Edit</button>

                        <form action="deleteNilaiMurid/{{ $item->id_nilai }}" method="POST" onsubmit="return confirm('Yakin menghapus nilai siswa ?')">
                            @csrf
                            @method('delete')
                                <button class="btn btn-danger" type="submit">Hapus</button>
                        </form>
                    
                </td>
            </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('modal')

        {{-- Modal Tambah Nilai --}}
        <div class="modal fade" id="modalTambahNilai" tabindex="-1" role="dialog" aria-labelledby="modalTambahNilai" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="modalTambahNilai">Tambah Data</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ URL::to('/createNilai') }}" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        @csrf
                                        <input type="hidden" name="nip" value="{{ auth()->user()->nomor_identitas }}">
                                        <label for="nama">Nis Siswa</label>
                                        <input type="text" class="form-control" id="nis" placeholder="Masukan nis siswa" name="nis">
                                    </div>
                                    <div class="form-group">
                                        <label for="uas">Ulangan Akhir Semester</label>
                                        <input type="text" class="form-control" id="uas" placeholder="Masukan Nilai UAS" name="uas">
                                    </div>
                                    <div class="form-group">
                                        <label for="uh">Ulangan Harian</label>
                                        <input type="text" class="form-control" id="uh" placeholder="Masukan Nilai UH" name="uh">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kelas">Kelas</label>
                                        <select name="kelas" id="kelas" class="form-control">
                                            <option selected hidden value="NULL">--Pilih--</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="uts">Ulangan Tengah Semester</label>
                                        <input type="text" class="form-control" id="uts" placeholder="Masukan Nilai UTS" name="uts">
                                    </div>
                                    <div class="form-group">
                                        <label for="mapel">Mata Pelajaran</label>
                                        <select name="mapel" class="form-control" id="mapel">
                                            <option selected hidden value="NULL">--Pilih--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 justify-content-center">
                                    <div class="form-group">
                                        <label for="total">Total</label>
                                        <input type="text" name="total" id="total" class="form-control">
                                    </div>
                                </div>

                            </div>


                        
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>


                {{-- Modal Edit Data --}}
                <div class="modal fade" id="modalEditNilai" tabindex="-1" role="dialog" aria-labelledby="modalEditNilai" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="modalEditNilai">Edit Data</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                            <form action="{{ URL::to('/editNilai') }}/" method="post" id="editFormNilai">
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @csrf
                                            <input type="hidden" name="nip" value="{{ auth()->user()->nomor_identitas }}">
                                            <label for="nama">Nis Siswa</label>
                                            <input type="text" class="form-control" id="editnis" placeholder="Masukan nis siswa" name="nis">
                                        </div>
                                        <div class="form-group">
                                            <label for="uas">Ulangan Akhir Semester</label>
                                            <input type="text" class="form-control" id="edituas" placeholder="Masukan Nilai UAS" name="uas">
                                        </div>
                                        <div class="form-group">
                                            <label for="uh">Ulangan Harian</label>
                                            <input type="text" class="form-control" id="edituh" placeholder="Masukan Nilai UH" name="uh">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="kelas">Kelas</label>
                                            <select name="kelas" id="editkelas" class="form-control">
                                                <option selected hidden value="NULL">--Pilih--</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="edituts">Ulangan Tengah Semester</label>
                                            <input type="text" class="form-control" id="edituts" placeholder="Masukan Nilai UTS" name="uts">
                                        </div>
                                        <div class="form-group">
                                            <label for="mapel">Mata Pelajaran</label>
                                            <select name="mapel" class="form-control" id="editmapel">
                                                <option selected hidden value="NULL">--Pilih--</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 justify-content-center">
                                        <div class="form-group">
                                            <label for="total">Total</label>
                                            <input type="text" name="total" id="edittotal" class="form-control">
                                        </div>
                                    </div>
    
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>

                <div class="modal fade" id="modalSiswa" tabindex="-1" role="dialog" aria-labelledby="modalSiswa" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="modalDataSiswa">Data Siswa</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="modalSiswaData">
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        </form>
                        </div>
                    </div>
                    </div>
@endsection

@section('footer')
    <p>Silakan menambahkan data disini</p>
@endsection

@section('scriptjs')
    <script>
        $(document).ready(function(){
        $("#datatable").DataTable({
            "language" : {
                "emptyTable" : "Tidak ada data",
                "zeroRecords" : "Tida ada data"
            }
        });

        var nip = {{auth()->user()->nomor_identitas}};

        $.ajax({
            url : "{{ URL::to('/getDataByWhere') }}/"+nip,
            type : "GET",
            success : function(res){
                var mapel = res[0].mapel;
                var splitter = mapel.split(',');
                var arrKosong = [];
                var arrKosong2 = [];
                var kelas = res[0].kelas;
                var splitter_kel = kelas.split(',');

                splitter_kel.forEach(nilai => {
                    $.ajax({
                        url : "{{ URL::to('/getDataByKelas') }}/"+nilai,
                        type : "GET",
                        success:function(res){
                            arrKosong2.push(res[0].id_kelas+"_"+res[0].kelas+" "+res[0].id_prodi+" "+res[0].nomor_kelas);

                            
                        } 
                    });
                });

                

                setTimeout(() => {
                    var sorter = Array.from(arrKosong2).sort();

                    console.log(sorter)
                     sorter.forEach(element => {
                         var ambilID = element.substr(0,2);
                         var ambilNama = element.substr(3,99);
                         $("#kelas,#editkelas").append(`
                            <option value="`+ambilID+`">`+ambilNama+`</option>
                         `);
                     });
                }, 1000);

                

                

                splitter.forEach(element => {
                   $.ajax({
                        url : "{{ URL::to('/getDataByMapel/') }}/"+element,
                        type : "GET",
                        success : function(data){
                            arrKosong.push(data[0].id_mapel+"_"+data[0].nama_mapel);
                        },
                        error: function(data){
                            alert("Error Code : #02923029");
                        }
                    });
                });
                setTimeout(() => {
                    var sorter = Array.from(arrKosong).sort();

                   sorter.forEach(element => {
                        var ambilNo = element.substr(0,1);
                        var string = element.substr(2,99);
                       $("#mapel,#editmapel").append(`
                            <option value="`+ambilNo+`">`+string+`</option>
                       `);
                   });
                }, 1000);
            },
            error : function(data){
                alert("Error Code : #149250");
            }
        });
    });


    $("#btnModalTrigger").click(function(){
        $("#modalTambahNilai").modal("show");

        
    });

    $('#modalTambahNilai,#modalEditNilai').on('hidden.bs.modal', function (e) {
      $(this)
      .find("input[type=text],input[type=password]")
        .val('')
        .end()

      .find("select").val('NULL').change().end()
      .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
    });
    

    $("#uas,#uts,#uh").keyup(function(){
        var uas = parseInt($("#uas").val());
        var uts = parseInt($("#uts").val());
        var uh = parseInt($("#uh").val());
        var total = (uh+uts+uas)/3;
        

        if(total == NaN){
            $("#total").val("Menghitung..");
        }else{
            $("#total").val(total.toFixed(1));
        }
    });

    $("#edituas,#edituts,#edituh").keyup(function(){
        var uas = parseInt($("#edituas").val());
        var uts = parseInt($("#edituts").val());
        var uh = parseInt($("#edituh").val());
        var total = (uh+uts+uas)/3;
        

        if(total == NaN){
            $("#edittotal").val("Menghitung..");
        }else{
            $("#edittotal").val(total.toFixed(1));
        }
    });

    
    function getNilaiByNis(nis){
        $("#modalEditNilai").modal("show");
        $("#editFormNilai").attr('action','{{ URL::to("/editNilai/") }}/'+nis);
        $.ajax({
            url : "{{ URL::to('/getNilai/') }}/"+nis,
            type : "GET",

            success:function(res){
                console.log(res);

                $("#editnis").val(res[0]["nis"]);
                $("#editkelas").val(res[0]["id_kelas"]).change();
                $("#edituas").val(res[0]["uas"]);
                $("#edituts").val(res[0]["uts"]);
                $("#edituh").val(res[0]["uh"]);
                $("#editmapel").val(res[0]["id_mapel"]).change();
                $("#edittotal").val(res[0]["total"]);
            },
            error : function(xhr){
                console.log(xhr);
            }
        });
    }
    

    $("#nis,#editnis").click(function(){

        if(this.id === "nis"){
            var id_kelas = $("#kelas").children("option:selected").val();
            if(id_kelas === "NULL"){
                 alert("PILIH KELAS NYA TERLEBIH DAHULU!!!");
            }else if(id_kelas != "NULL"){
                $("#modalSiswa").modal("show");
            }
            
        }else if(this.id === "editnis"){
            var id_kelas = $("#editkelas").children("option:selected").val();
            if(id_kelas === "NULL"){
                 alert("PILIH KELAS NYA TERLEBIH DAHULU!!!");
            }else if(id_kelas != "NULL"){
                $("#modalSiswa").modal("show");
            }
        }
        
        
        
        $.ajax({
            url : "{{ URL::to('/getMuridByIdKelas/') }}/" + id_kelas,
            type : "GET",

            success : function(response){

                $("#modalSiswaData").append(`
                    <table class="tableSiswa" id="tableSiswa">
                        <thead>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>JK</th>
                        </thead>
                        <tbody id="tbodyIsiData">

                        </tbody>
                    </table>
                `);

                $.each(response,function(i,v){
                    $("#tbodyIsiData").append(`
                        <tr>
                            <td>
                                `+response[i].nis+`
                            </td>
                            <td>
                                `+response[i].name+`
                            </td>
                            <td>
                                `+response[i].jk_siswa+`    
                            </td>
                        </tr>
                    `);
                });

                setTimeout(() => {
                    var tableData = $(".tableSiswa").DataTable({
                        "language" : {
                            "emptyTable" : "Tidak ada data",
                            "zeroRecords" : "Tida ada data"
                        }
                        
                    });

                    $(".tableSiswa").click(function(){
                        var currow = $(this).closest("tr");
                        var rows = tableData.rows(0).data();
                        var getNis = rows[0][0];

                        $("#nis").val(getNis);

                        $("#modalSiswa").modal("hide");



                    });

                    
                }, 500);
                
                    
               
            },
            error : function(xhr){
                console.log(xhr);
            }
            
        });
        
        $("#modalSiswaData,.tableSiswa").html("");


        
        
        
        
    });

    </script>
@endsection
