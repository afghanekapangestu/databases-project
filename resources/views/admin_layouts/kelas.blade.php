@extends('dashboard')

@section('content-header')
<h1>
        Tambah Kelas
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Kelas</li>
      </ol>
@endsection

@section('header_button')
    <h3 class="box-title">Data Kelas</h3>
    <button class="btn btn-success" id="btnModalTrigger">(+) Tambah Kelas</button>
@endsection

@section('table')

<table class="table table-bordered table-striped dataTable text-center" id="datatable">
        <thead>
              <th>#</th>
              <th>Kelas</th>
              <th>Action</th>
        </thead>
        <tbody>
          @foreach ($data as $d)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$d->kelas}}&nbsp;{{$d->id_prodi}}&nbsp;{{$d->nomor_kelas}}
                </td>
                <td>
                        <form action="{{URL::to('/deletekelas')}}/{{$d->id_kelas}}" method="POST">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btnEdit" type="button" onclick="showModalEditKelas('{{$d->id_kelas}}')">Edit</button>
                            <button class="btn btn-danger" type="submit">Hapus</button>
                        </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
@endsection

@section('modal')

        {{-- Modal Tambah Kelas --}}
        <div class="modal fade" id="modalTambahKelas" tabindex="-1" role="dialog" aria-labelledby="modalTambahData" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="modalTambahKelas">Tambah Kelas</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{URL::to('/createkelas')}}" method="POST">
                        <div class="form-group">
                                @csrf
                                <label for="kelas_grup">Kelas</label>
                                <select name="id_kelasgrup" id="id_kelasgrup" class="form-control">
                                    @foreach ($data2 as $item)
                                        <option value="{{$item->id_kelasgrup}}">{{$item->kelas}}</option>
                                    @endforeach
                                </select><br>

                                <label for="jurusan">Jurusan</label>
                                <select name="id_prodi" id="id_prodi" class="form-control">
                                    @foreach ($data1 as $item)
                                        <option value="{{$item->id_prodi}}">{{$item->nama_prodi}}</option>
                                    @endforeach
                                </select><br>

                                <label for="nomor">Nomor</label>
                                <select name="nomor" id="nomor" class="form-control">
                                    @foreach ($data3 as $item)
                                        <option value="{{$item->id_nomor}}">{{$item->nomor_kelas}}</option>
                                    @endforeach
                                </select>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>



        {{-- Modal Edit Kelas --}}
        <div class="modal fade" id="modalEditKelas" tabindex="-1" role="dialog" aria-labelledby="modalEditData" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="modalEditKelas">Edit Kelas</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{URL::to('/editkelas')}}" method="POST">
                    <div class="form-group">
                            @csrf
                            @method('put')

                            <input type="hidden" id="id_kelas" name="id_kelas">
                            <label for="kelas_grup">Kelas</label>
                            <select name="id_kelasgrup" id="id_kelasgrupedit" class="form-control">

                                @foreach ($data2 as $item)
                                    <option value="{{$item->id_kelasgrup}}">{{$item->kelas}}</option>
                                @endforeach
                            </select><br>

                            <label for="jurusan">Jurusan</label>
                            <select name="id_prodi" id="id_prodiedit" class="form-control">

                                @foreach ($data1 as $item)
                                    <option value="{{$item->id_prodi}}">{{$item->nama_prodi}}</option>
                                @endforeach
                            </select><br>

                            <label for="nomor">Nomor</label>
                            <select name="nomor" id="nomoredit" class="form-control">
                                @foreach ($data3 as $item)
                                    <option value="{{$item->id_nomor}}">{{$item->nomor_kelas}}</option>
                                @endforeach
                            </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </div>
                </form>
                </div>
            </div>
            </div>



@endsection

@section('scriptjs')
<script>
    $(document).ready(function(){
    $("#datatable").DataTable();
});

$("#btnModalTrigger").click(function(){
    $("#modalTambahKelas").modal("show");
});



function showModalEditKelas(id_kelas){
    $("#modalEditKelas").modal("show");

    $.ajax({
        url : "{{URL::to('/getdatakelasedit/')}}/" + id_kelas,
        type : "GET",
        success : function(response){
            console.table(response);

            $("#id_kelasgrupedit").val(response[0]["id_kelasgrup"]).change();
            $("#id_prodiedit").val(response[0]["id_prodi"]).change();
            $("#id_kelas").val(response[0]["id_kelas"]);


            console.log("alert")

        },
        error:function(response){
            console.log(response);
        }
    });
}

</script>


@endsection
