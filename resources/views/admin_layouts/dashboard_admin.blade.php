@extends('dashboard')


@section('content-header')
<h1>
    Dashboard
</h1>

      <ol class="breadcrumb">
        <li>
            <a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
        </li>
      </ol>

      <br>

      <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box mt-2">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-red"><i class="fa fa-user-secret"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Admin</span>
                <span class="info-box-number">{{$admin}}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box mt-2">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-green"><i class="fa  fa-user"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Guru</span>
                <span class="info-box-number">{{$guru}}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div>

          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box mt-2">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-yellow"><i class="fa  fa-user"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Siswa</span>
                <span class="info-box-number">{{$siswa}}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box mt-2">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-purple"><i class="fa  fa-users"></i></span>
                <div class="info-box-content">
                <span class="info-box-text">Total Akun</span>
                <span class="info-box-number">{{$all}}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
          </div>
      </div>

@endsection
