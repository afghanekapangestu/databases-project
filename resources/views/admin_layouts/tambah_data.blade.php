@extends('dashboard')

@section('content-header')
      <h1>
        Tambah Data
      </h1>

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Data</li>
      </ol>
@endsection

@section('header_button')
    <h3 class="box-title">Data Akun</h3>
    <button class="btn btn-success" id="btnModalTrigger">(+) Tambah User</button>
@endsection

@section('table')


    @if ($message = Session::get('status'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif


<table class="table table-bordered table-striped dataTable text-center" id="datatable">
        <thead>
              <th>#</th>
              <th>Nomor Identitas</th>
              <th>Nama</th>
              <th>Role</th>
              <th>Email</th>
              <th>Action</th>
        </thead>
        <tbody>
          @foreach ($data as $item)
            <tr>
                <td>
                    {{$loop->iteration}}
                </td>
                <td>
                    {{$item->nomor_identitas}}
                </td>
                <td>
                    {{$item->name}}
                </td>
                <td>
                    {{$item->role}}
                </td>
                <td>
                    {{$item->email}}
                </td>
                <td>

                      <form action="{{URL::to('/deleteuser')}}/{{$item->id}}" method="POST" onsubmit="return confirm('Yakin ingin hapus?')">
                          @csrf
                          @method('delete')
                          <button class="btn btn-primary btnEdit" type="button" onclick="geteditdata('{{$item->role}}',{{$item->nomor_identitas}});">Edit</button>
                          <button class="btn btn-danger" type="submit">Hapus</button>
                      </form>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
@endsection

@section('modal')

        {{-- Modal Tambah Data --}}
        <div class="modal fade" id="modalTambahData" tabindex="-1" role="dialog" aria-labelledby="modalTambahData" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="modalTambahData">Tambah Data</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{URL::to('/createuser')}}" id="formTambahData">
                            @csrf
                            <div class="form-group">
                                <label for="name">Role</label>
                                <select name="role" id="role" class="form-control">
                                    <option selected="selected" hidden>--Pilih--</option>
                                    <option value="admin">Admin</option>
                                    <option value="guru">Guru</option>
                                    <option value="siswa">Siswa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <label for="name">Nomor Identitas</label>
                                <input type="text" class="form-control" id="nomor_identitas" name="nomor_identitas" placeholder="Masukan Nomor Identitas">
                            </div>

                            <div class="form-group jenis_kelamin" >
                                <label for="name">Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="jk" class="form-control">
                                    <option selected="selected" hidden>--Pilih--</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Masukan email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                            </div>

                            <div class="form-check checkBoxMapel" style="display:none;" >
                                <label for="mapel">Mapel</label><br>
                                @foreach ($mapel as $m)
                                    <input class="form-check-input" type="checkbox" value="{{ $m->id_mapel }}"  name="mapel[]" id="{{$m->nama_mapel}}">
                                    <label class="form-check-label" for="{{$m->nama_mapel}}">
                                        {{$m->nama_mapel}}
                                    </label><br>
                                @endforeach

                            </div>
                            <div class="form-group kelasMengajar">
                                <label for="kelas">Kelas Mengajar</label>
                                <select id="kelasMengajar">
                                    @foreach ($kelas as $k)
                                        <option value="{{$k->id_kelas}}">{{$k->kelas}} {{$k->id_prodi}} {{$k->nomor_kelas}}</option>
                                    @endforeach
                                </select>

                                <p>Selected Kelas : <span id="hasilSelectedKelas"></span></p>
                                <input type="hidden" value="" id="selectedKelas" name="kelasMengajar">

                                <div class="row rowSelectedKelas">


                                </div>
                                <div style="display:none" id="clearKelasMengajar">
                                    <button type="button" class="btn btn-warning" id="btnKelasMengajar">Clear Kelas Mengajar</button>
                                </div>
                            </div>

                            <div class="form-group optionKelas" style="display:none;">
                                <label for="kelas">Kelas</label>
                                <select name="kelas" id="kelas" class="form-control">
                                    <option selected="selected" hidden>--Pilih--</option>
                                    @foreach ($kelas as $k)
                                        <option value="{{$k->id_kelas}}">{{$k->kelas}} {{$k->id_prodi}} {{$k->nomor_kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form_alamat">
                                <label for="alamat">Alamat</label>
                                <textarea name="alamat" id="alamat" cols="15" rows="5" class="form-control" style="resize:none;"></textarea>
                            </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>


                {{-- Modal Edit Data --}}
                <div class="modal fade" id="modalEditData" tabindex="-1" role="dialog" aria-labelledby="modalEditData" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="modalEditData">Edit Data</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{URL::to('/edituser')}}" id="formEditData">
                            @csrf
                            @method('PUT')

                            <input type="hidden" name="id" id="inputEditHidden">
                            <div class="form-group">
                                <label for="name">Role</label>
                                <select name="role" id="editrole" class="form-control">
                                    <option selected hidden>--Pilih--</option>
                                    <option value="admin">Admin</option>
                                    <option value="guru">Guru</option>
                                    <option value="siswa">Siswa</option>
                                </select>
                            </div>
                           <div class="form-group">
                                <label for="name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="editname" name="name" placeholder="Masukan Nama Lengkap">
                            </div>
                            <div class="form-group">
                                <label for="name">Nomor Identitas</label>
                                <input type="text" class="form-control" id="editnomor_identitas" name="nomor_identitas" placeholder="Masukan Nomor Identitas">
                            </div>

                            <div class="form-group jenis_kelamin" >
                                <label for="name">Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="editjk" class="form-control">
                                    <option selected hidden>--Pilih--</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="editemail" name="email" aria-describedby="emailHelp" placeholder="Masukan email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="editpassword" placeholder="Password" name="password">
                            </div>

                            <div class="form-check checkBoxMapel" style="display:none;" >
                                <label for="mapel">Mapel</label><br>

                                @foreach ($mapel as $m)

                                    <input class="form-check-input inputCheckBox" type="checkbox" value="{{ $m->id_mapel }}"  name="mapel[]" id="edit{{$m->id_mapel}}">
                                    <label class="form-check-label" for="{{$m->nama_mapel}}">
                                        {{$m->nama_mapel}}
                                    </label><br>
                                @endforeach

                            </div>

                            <div class="form-group kelasMengajar">
                                <label for="kelas">Kelas Mengajar</label>
                                <select id="editkelasMengajar" class="form-control">
                                    @foreach ($kelas as $k)
                                        <option value="{{$k->id_kelas}}">{{$k->kelas}} {{$k->id_prodi}} {{$k->nomor_kelas}}</option>
                                    @endforeach
                                </select>

                                <p>Selected Kelas : <span id="editHasilSelectedKelas"></span></p>

                                <input type="hidden" value="" id="editSelectedKelas" name="editkelasMengajar">
                                <div style="display:none" id="editClearKelasMengajar">
                                    <button type="button" class="btn btn-warning" id="editBtnKelasMengajar">Clear Kelas Mengajar</button>
                                </div>
                            </div>

                            <div class="form-group optionKelas" style="display:none;" >
                                <label for="kelas">Kelas</label>
                                <select name="kelas" id="editkelas" class="form-control">

                                    @foreach ($kelas as $k)
                                        <option value="{{$k->id_kelas}}">{{$k->kelas}}&nbsp;{{$k->id_prodi}}&nbsp;{{$k->nomor_kelas}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form_alamat">
                                <label for="alamat">Alamat</label>
                                <textarea name="alamat" id="editalamat" cols="15" rows="5" class="form-control" style="resize:none;"></textarea>
                            </div>



                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                    </form>
                    </div>
                </div>
                </div>
@endsection

@section('footer')
    <p>Silakan menambahkan data disini</p>
@endsection

@section('scriptjs')
    <script>
        $(document).ready(function(){
            $('#kelasMengajar,#editkelasMengajar').select2({
                theme: 'bootstrap4',
                width:'style'
            });
            $("#datatable").DataTable({
                "language" : {
                    "emptyTable" : "Tidak ada data"
                }
            });
        });

    var arraySelectedKelas = new Array();
    var arraySelectedKelas2 = new Array();
    $("#kelasMengajar").change(function(){
        var selectedtext = $("#kelasMengajar option:selected").html();
        var valueSelected = $(this).val();

        $("#hasilSelectedKelas").html("");
        $("#selectedKelas").val("");


        setTimeout(() => {
            $("#hasilSelectedKelas").append(arraySelectedKelas2.join(','));
            $("#selectedKelas").val(arraySelectedKelas.join(','));
        }, 1000);




        arraySelectedKelas.push(valueSelected);
        arraySelectedKelas2.push(selectedtext);




        $("#btnKelasMengajar").click(function(){
            $("#hasilSelectedKelas").text("");
            $("#kelasMengajar").prop('disabled',false);
            arraySelectedKelas.length = 0;
            arraySelectedKelas2.length = 0;
            $("#btnKelasMengajar").hide();
        });

        if(arraySelectedKelas.length > 3){


            alert("Kelas tidak boleh melebihi dari 3");
            arraySelectedKelas.length = 0;
            arraySelectedKelas2.length = 0;
        }
        if(arraySelectedKelas.length == 3){
            $("#clearKelasMengajar").show();
            $("#kelasMengajar").prop('disabled',true);
        }
    });

    $("#btnModalTrigger").click(function(){
        $("#modalTambahData").modal("show");
        $(".form_alamat").show();
        $("#role").val("--Pilih--").change();
    });

    $('#modalTambahData,#modalEditData').on('hidden.bs.modal', function (e) {
      $(this)
      .find("input[type=text],input[type=password]")
        .val('')
        .end()

      .find("select").val('--Pilih--').end()
      .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
    });


    $("#role,#editrole").change(function(){
        var value = $(this).val();

        if(value == "guru"){
            $(".checkBoxMapel").show();
            $(".optionKelas").hide();
            $(".jenis_kelamin").show();
            $(".form_alamat").show();
            $(".kelasMengajar").show();
        }else if(value == "siswa"){
            $(".checkBoxMapel").hide();
            $(".optionKelas").show();
            $(".form_alamat").show();
            $(".jenis_kelamin").show();
            $(".kelasMengajar").hide();
        }else{
            $(".checkBoxMapel").hide();
            $(".optionKelas").hide();
            $('.jenis_kelamin,.form_alamat').hide();
            $(".kelasMengajar").hide();
        }
    });

    function geteditdata(role,nomor_identitas){
        $("#modalEditData").modal("show");
        var url = $("#formEditData").attr('action',"{{URL::to('edituser/')}}/"+nomor_identitas);
        var editArraySelectedKelas = new Array();


        $("#editalamat").val("");
        $.ajax({
            type: "GET",
            url : "{{URL::to('getdatauser/')}}/"+role+"/"+nomor_identitas,

            success:function(response){
                    $("#editname").val(response[0]["name"]);
                    $("#inputEditHidden").val(response[0]["id"]);
                    $("#editnomor_identitas").val(response[0]["nomor_identitas"]);
                    $("#editrole").val(response[0]["role"]).change().attr('selected',true);
                    $("#editemail").val(response[0]["email"]);
                    $("#editkelas").val(response[0]["id_kelas"]).change().attr('selected',true);
                    $("#editpassword").val(response[0]["password"]);
                    if(role == "siswa"){
                        $("#editalamat").val(response[0]["alamat_siswa"]);
                        $("#editname").val(response[0]["name"]);
                        $("#editjk").val(response[0]["jk_siswa"]).change().attr('selected',true);
                        $('.jenis_kelamin, .form_alamat').show();
                    }else if(role == "guru"){
                        var kelas = response[0]["kelas"];
                        var kelasSelected = kelas.split(',');
                        editArraySelectedKelas = kelasSelected.slice(0);

                        // $("#editkelasMengajar").prop('disabled',true);
                        // $("#editClearKelasMengajar").show();


                        $("#editkelasMengajar").change(function(){
                            var selectedtext = $("#editkelasMengajar option:selected").html();
                            var valueSelected = $(this).val();

                            $("#editHasilSelectedKelas").html("");
                            $("#editSelectedKelas").val("");

                            setTimeout(() => {
                                $("#editHasilSelectedKelas").append(editArraySelectedKelas.join(','));
                                $("#editSelectedKelas").val(editArraySelectedKelas.join(','));
                            }, 1000);



                            editArraySelectedKelas.push(valueSelected);


                            $("#editBtnKelasMengajar").click(function(){
                                $("#editHasilSelectedKelas").text("");
                                $("#editkelasMengajar").prop('disabled',false);
                                editArraySelectedKelas.length = 0;
                                $("#editBtnKelasMengajar").hide();
                            });

                            if(editArraySelectedKelas.length > 3){
                                alert("Kelas tidak boleh melebihi dari 3");
                                alert("Pilih Kelas Mengajar lagi");
                                editArraySelectedKelas.length = 0;
                            }
                            if(editArraySelectedKelas.length == 3){
                                $("#editClearKelasMengajar").show();
                                $("#editkelasMengajar").prop('disabled',true);
                            }
                        });
                        var getMapelArr = []
                        var getMapel = response[0]["mapel"]
                        var getMapelEx = getMapel.split(',');
                        $("#editSelectedKelas").val(response[0]["kelas"]);
                        $("#editHasilSelectedKelas").text(response[0]["kelas"]);

                        for(i=0;i<getMapelEx.length;i++){
                            getMapelArr.push(getMapelEx[i]);

                        }
                        $(".inputCheckBox").each(function(){

                            var a = this.id;
                            var b = $("#"+a).val();

                            if(b == getMapelArr[0] || b == getMapelArr[1] || b == getMapelArr[2]){
                                $("#"+a).prop('checked',true);
                            }
                            });

                        $("#editalamat").val(response[0]["alamat_guru"]);
                        $("#editjk").val(response[0]["jk"]).change();
                        $('.jenis_kelamin , .form_alamat').show();

                    }else if(role == "admin"){
                        $('.jenis_kelamin,.form_alamat').hide();
                    }
            },
            error:function(response){
                alert("Error 4120");
            }
        });
    }
    </script>



    <script>
            /**
        * Disable right-click of mouse, F12 key, and save key combinations on page
        * By Arthur Gareginyan (arthurgareginyan@gmail.com)
        * For full source code, visit https://mycyberuniverse.com
        */
    // window.onload = function() {
    //     document.addEventListener("contextmenu", function(e){
    //     e.preventDefault();
    //     }, false);
    //     document.addEventListener("keydown", function(e) {
    //     //document.onkeydown = function(e) {
    //     // "I" key
    //     if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
    //         disabledEvent(e);
    //     }
    //     // "J" key
    //     if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
    //         disabledEvent(e);
    //     }
    //     // "S" key + macOS
    //     if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
    //         disabledEvent(e);
    //     }
    //     // "U" key
    //     if (e.ctrlKey && e.keyCode == 85) {
    //         disabledEvent(e);
    //     }
    //     // "F12" key
    //     if (event.keyCode == 123) {
    //         disabledEvent(e);
    //     }
    //     }, false);
    //     function disabledEvent(e){
    //     if (e.stopPropagation){
    //         e.stopPropagation();
    //     } else if (window.event){
    //         window.event.cancelBubble = true;
    //     }
    //     e.preventDefault();
    //     return false;
    //     }
    // };
    </script>
@endsection
