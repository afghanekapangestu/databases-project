-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Nov 2019 pada 09.17
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databases`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `nip` varchar(255) NOT NULL,
  `alamat_guru` text NOT NULL,
  `jk` enum('P','L') NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `kelas` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`nip`, `alamat_guru`, `jk`, `mapel`, `kelas`) VALUES
('1017007668', '123', 'L', '1,7,2', '59,60,61');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(12) NOT NULL,
  `id_prodi` varchar(12) NOT NULL,
  `id_kelasgrup` int(11) NOT NULL,
  `id_nomor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_prodi`, `id_kelasgrup`, `id_nomor`) VALUES
(59, 'BKP', 1, 1),
(60, 'BKP', 1, 2),
(61, 'BKP', 1, 3),
(62, 'BKP', 2, 1),
(63, 'BKP', 2, 2),
(64, 'BKP', 2, 3),
(65, 'BKP', 3, 1),
(66, 'BKP', 3, 2),
(67, 'BKP', 3, 3),
(68, 'MM', 1, 1),
(69, 'MM', 1, 2),
(70, 'MM', 1, 3),
(71, 'MM', 2, 1),
(72, 'MM', 2, 2),
(73, 'MM', 2, 3),
(74, 'MM', 3, 1),
(75, 'MM', 3, 2),
(76, 'MM', 3, 3),
(77, 'RPL', 1, 1),
(78, 'RPL', 1, 2),
(79, 'RPL', 1, 3),
(80, 'RPL', 2, 1),
(81, 'RPL', 2, 2),
(82, 'RPL', 2, 3),
(83, 'RPL', 3, 1),
(84, 'RPL', 3, 2),
(85, 'RPL', 3, 3),
(86, 'TKJ', 1, 1),
(87, 'TKJ', 1, 2),
(88, 'TKJ', 1, 3),
(89, 'TKJ', 2, 1),
(90, 'TKJ', 2, 2),
(91, 'TKJ', 2, 3),
(92, 'TKJ', 3, 1),
(93, 'TKJ', 3, 2),
(94, 'TKJ', 3, 3),
(95, 'TFLM', 1, 1),
(96, 'TFLM', 1, 2),
(97, 'TFLM', 1, 3),
(98, 'TFLM', 2, 1),
(99, 'TFLM', 2, 2),
(100, 'TFLM', 2, 3),
(101, 'TFLM', 3, 1),
(102, 'TFLM', 3, 2),
(103, 'TFLM', 3, 3),
(104, 'TGB', 1, 1),
(105, 'TGB', 1, 2),
(106, 'TGB', 1, 3),
(107, 'TGB', 2, 1),
(108, 'TGB', 2, 2),
(109, 'TGB', 2, 3),
(110, 'TGB', 3, 1),
(111, 'TGB', 3, 2),
(112, 'TGB', 3, 3),
(113, 'TOI', 1, 1),
(114, 'TOI', 1, 2),
(115, 'TOI', 1, 3),
(116, 'TOI', 2, 1),
(117, 'TOI', 2, 2),
(118, 'TOI', 2, 3),
(119, 'TOI', 3, 1),
(120, 'TOI', 3, 2),
(121, 'TOI', 3, 3),
(122, 'TKR', 1, 1),
(123, 'TKR', 1, 2),
(124, 'TKR', 1, 3),
(125, 'TKR', 2, 1),
(126, 'TKR', 2, 2),
(127, 'TKR', 2, 3),
(128, 'TKR', 3, 1),
(129, 'TKR', 3, 2),
(130, 'TKR', 3, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas_grup`
--

CREATE TABLE `kelas_grup` (
  `id_kelasgrup` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas_grup`
--

INSERT INTO `kelas_grup` (`id_kelasgrup`, `kelas`) VALUES
(1, 'X'),
(2, 'XI'),
(3, 'XII');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(12) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`) VALUES
(1, 'Bahasa Indonesia'),
(2, 'Bahasa Inggris'),
(3, 'Bahasa Sunda'),
(4, 'Pendidikan Agama dan Budi Pekerti'),
(5, 'PPKn'),
(6, 'Matematika'),
(7, 'Seni Budaya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(12) NOT NULL,
  `id_mapel` int(12) NOT NULL,
  `id_kelas` int(12) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `uas` varchar(35) NOT NULL,
  `uh` varchar(35) NOT NULL,
  `uts` varchar(35) NOT NULL,
  `total` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_mapel`, `id_kelas`, `nis`, `nip`, `uas`, `uh`, `uts`, `total`) VALUES
(5, 1, 59, '1017007667', '1017007668', '98', '54', '97', '83.0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nomor_kelas`
--

CREATE TABLE `nomor_kelas` (
  `id_nomor` int(11) NOT NULL,
  `nomor_kelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nomor_kelas`
--

INSERT INTO `nomor_kelas` (`id_nomor`, `nomor_kelas`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` varchar(10) NOT NULL,
  `nama_prodi` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `nama_prodi`) VALUES
('BKP', 'Bisnis Konstruksi & Properti'),
('DPIB', 'Desain Permodelan dan Informasi Bangunan'),
('MM', 'Multimedia'),
('RPL', 'Rekayasa Perangkat Lunak'),
('SIJA', 'Sistem Informasi Jaringan Aplikasi'),
('TFLM', 'Teknik Fabrikasi Logam & Manufakturing'),
('TGB', 'Teknik Gambar Bangunan'),
('TKJ', 'Teknik Komputer dan Jaringan'),
('TKR', 'Teknik Kendaraan Ringan'),
('TOI', 'Teknik Otomasi Industri');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(255) NOT NULL,
  `alamat_siswa` text NOT NULL,
  `jk_siswa` enum('P','L') NOT NULL,
  `id_kelas` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `alamat_siswa`, `jk_siswa`, `id_kelas`) VALUES
('1017007667', '123', 'P', 59);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_identitas` varchar(255) CHARACTER SET latin1 NOT NULL,
  `role` enum('guru','siswa','admin','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `nomor_identitas`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(47, 'Afghan Eka Pangestu', '1017007669', 'admin', 'afghanekapangestu@gmail.com', NULL, '$2y$12$om6d0fMqPOb1RvoxmbwQwe1B32jYBdYYCvHy6TosEbZlQ40qB0wB.', 'VBzhGuYeoR3JXjK3VanSZ6U7g0uTQx1ZOE4D6MBTRT5C8RKrL7LtTOR87qZW', NULL, NULL),
(53, 'Adinda Lailatul', '1017007668', 'guru', 'adindalailatul060802@gmail.com', NULL, '$2y$10$k58YenyvYY5EB10s2K0p5OaGYhdhynv2Mi9UEh6l5Z51LjVl/Pur6', 'U9tW4uCK0R54jnurJJFbxikmpY7mDKJqnhrUYQvlBtBiDngAcW5vleBrEPQM', '2019-11-16 18:44:46', '2019-11-16 18:44:46'),
(56, 'Achmad Rizqullah Blessar', '1017007667', 'siswa', 'achmadrizqullah@gmail.com', NULL, '$2y$10$iFxTAA4LH0Hl4By0htpHjuHZ22u2ba9mmFQr29XAvKTwPv6A3sX.m', 'ARWWRlmQSmIAaeG67iEqko87ANITk4EMxNOdSB6En85Iiz0vNLI4Hm4WnKUL', '2019-11-16 18:48:18', '2019-11-16 18:48:18');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_prodi` (`id_prodi`),
  ADD KEY `id_kelasgrup` (`id_kelasgrup`),
  ADD KEY `id_nomor` (`id_nomor`);

--
-- Indeks untuk tabel `kelas_grup`
--
ALTER TABLE `kelas_grup`
  ADD PRIMARY KEY (`id_kelasgrup`);

--
-- Indeks untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `nip` (`nip`),
  ADD KEY `nis` (`nis`) USING BTREE;

--
-- Indeks untuk tabel `nomor_kelas`
--
ALTER TABLE `nomor_kelas`
  ADD PRIMARY KEY (`id_nomor`),
  ADD KEY `nomor_kelas` (`nomor_kelas`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`),
  ADD UNIQUE KEY `nis` (`nis`) USING BTREE,
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nomor_identitas` (`nomor_identitas`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT untuk tabel `kelas_grup`
--
ALTER TABLE `kelas_grup`
  MODIFY `id_kelasgrup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `nomor_kelas`
--
ALTER TABLE `nomor_kelas`
  MODIFY `id_nomor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `users` (`nomor_identitas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`id_prodi`) REFERENCES `prodi` (`id_prodi`),
  ADD CONSTRAINT `kelas_ibfk_2` FOREIGN KEY (`id_kelasgrup`) REFERENCES `kelas_grup` (`id_kelasgrup`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_ibfk_3` FOREIGN KEY (`id_nomor`) REFERENCES `nomor_kelas` (`id_nomor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `nilai_ibfk_2` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`),
  ADD CONSTRAINT `nilai_ibfk_3` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_4` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `users` (`nomor_identitas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
