<?php


//  Login Routes
Route::get('/', 'AuthController@formLogin');
Auth::routes();

Route::post('/proseslogin', 'AuthController@prosesLogin');
Route::get('/logout', 'AuthController@logout');

// End Login Routes

// Route for global
Route::group(['middleware' => ['auth', 'checkRole:admin,guru,siswa']], function () { });

// Route for admin

Route::group(['middleware' => 'auth', 'checkRole:admin'], function () {
    Route::get('/dashboard_admin', 'AdminController@dashboard');
    Route::get('/dashboard_admin/tambah_data', 'MainController@getData');
    Route::post('/createuser', 'AdminController@CreateUser');
    Route::post('/createkelas', 'AdminController@createkelas');
    Route::delete('/deleteuser/{id}', 'AdminController@DeleteUser');
    Route::delete('/deletekelas/{id}', 'AdminController@deletekelas');
    Route::put('/edituser/{id}', 'AdminController@EditUser');
    Route::get('/getdatauser/{role}/{nomor_identitas}', 'AdminController@GetDataUser');
    Route::get('/dashboard_admin/kelas', 'MainController@getDataKelas');
    Route::get('/getdatakelasedit/{id_kelas}', 'MainController@getDataKelasEdit');
    Route::put('/editkelas', 'AdminController@editkelas');
});



// Route for guru
Route::group(['middleware' => 'auth', 'checkRole:guru'], function () {
    Route::get('/dashboard_guru', function () {
        return view('dashboard');
    });

    Route::get('/dashboard_guru/tambah_nilai', 'GuruController@getData');



    Route::post('/createNilai', 'GuruController@createNilai');

    Route::get('/getDataByWhere/{nip}', 'GuruController@getDataByWhere');

    Route::get('/getDataByMapel/{id_mapel}', 'GuruController@getDataByMapel');

    Route::get('/getDataByKelas/{id_kelas}', 'GuruController@getDataByKelas');

    Route::get('/getMuridByIdKelas/{id_kelas}', 'GuruController@getMuridByIdKelas');

    Route::delete('/dashboard_guru/deleteNilaiMurid/{id_nilai}', 'GuruController@deleteNilaiMurid');

    Route::get('/getNilai/{nis}', 'GuruController@getNilaiByNis');

    Route::put('/editNilai/{nis}', 'GuruController@editNilai');
});


// Route for siswa

Route::group(['middleware' => 'auth', 'checkRole:siswa'], function () {
    Route::get('/dashboard_siswa/{nis}', 'SiswaController@getNilaiSiswa');
});
