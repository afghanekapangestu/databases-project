<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';
    protected $fillable = [
        'id_nilai','id_mapel','id_kelas','nis','nip','uas','uh','uts','total'
    ];

    protected $primaryKey = 'id_nilai';
    public $timestamps = false;
}
