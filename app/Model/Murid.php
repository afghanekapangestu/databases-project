<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Murid extends Model
{
    //
    protected $table = 'siswa';

    protected $fillable = [
        'nis','alamat_siswa','jk_siswa','id_kelas'
    ];

    public $timestamps = false;
}
