<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';

    protected $fillable = [
        'nip', 'alamat_guru', 'jk', 'mapel', 'kelas'
    ];
    public $timestamps = false;
}
