<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $primaryKey = 'id_kelas';
    protected $fillable = [
        'id_kelas','id_prodi','id_kelasgrup','id_nomor'
    ];

    public $timestamps = false;
}
