<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Guru;
use App\Model\Murid;
use App\Model\Nilai;

class GuruController extends Controller
{
    public function createNilai(Request $r){
        Nilai::create([
            'id_mapel' => $r->mapel,
            'id_kelas' => $r->kelas,
            'nis' => $r->nis,
            'nip' => $r->nip,
            'uas' => $r->uas,
            'uh' => $r->uh,
            'uts' => $r->uts,
            'total' => $r->total
        ]);

        return redirect()->back()->with('status','Data berhasil ditambahkan');
    }

    public function editNilai($nis , Request $r){
        
        
        $data = Nilai::where('nis',$nis)->update([
            'id_mapel' => $r->mapel,
            'id_kelas' => $r->kelas,
            'nis' => $r->nis,
            'nip' => $r->nip,
            'uas' => $r->uas,
            'uh' => $r->uh,
            'uts' => $r->uts,
            'total' => $r->total
        ]);

        return redirect()->back()->with('status','Data berhasil diperbarui');
    }

    public function getData(){
        $data = \DB::table('nilai')
                ->join('mapel','nilai.id_mapel','=','mapel.id_mapel')
                ->join('kelas','nilai.id_kelas','=','kelas.id_kelas')
                ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
                ->join('nomor_kelas','kelas.id_nomor','=','nomor_kelas.id_nomor')
                ->join('siswa','nilai.nis','=','siswa.nis')
                ->join('users','users.nomor_identitas','=','nilai.nis')
                ->get();


        return view('guru_layouts.tambah_nilai',compact('data'));
    }

    public function getDataByWhere($nip){
        $dataWhere = Guru::where('nip',$nip)->get();

        return response()->json($dataWhere, 200);
    }

    public function getDataByMapel($id_mapel){
        $dataByMapel = \DB::table('mapel')->where('id_mapel',$id_mapel)->get();

        return response()->json($dataByMapel, 200);
    }

    public function getDataByKelas($id_kelas){
        $dataByKelas = \DB::table('kelas')
        ->join('prodi','kelas.id_prodi','=','prodi.id_prodi')
        ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
        ->join('nomor_kelas','nomor_kelas.id_nomor','=','kelas.id_nomor')
        ->where('id_kelas',$id_kelas)
        ->orderBy('id_kelas','ASC')
        ->get();

        return response()->json($dataByKelas, 200);
    }

    public function getMuridByIdKelas($id_kelas){
        $dataMurid = \DB::table('siswa')
                     ->join('users','siswa.nis','=','users.nomor_identitas')
                     ->orderBy('name','ASC')
                     ->where('id_kelas',$id_kelas)
                     ->get();

        return response()->json($dataMurid, 200);
    }

    public function deleteNilaiMurid($id_nilai){
        $data = Nilai::where('id_nilai',$id_nilai);
        $data->delete();

        return redirect()->back()->with('status','Berhasil menghapus nilai siswa');
    }

    public function getNilaiByNis($nis){
        $data = \DB::table('nilai')
                ->join('mapel','nilai.id_mapel','=','mapel.id_mapel')
                ->join('kelas','nilai.id_kelas','=','kelas.id_kelas')
                ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
                ->join('nomor_kelas','kelas.id_nomor','=','nomor_kelas.id_nomor')
                ->join('siswa','nilai.nis','=','siswa.nis')
                ->join('users','users.nomor_identitas','=','nilai.nis')
                ->where('nilai.nis',$nis)
                ->get();

        

        unset(

            $data[0]->password,
            $data[0]->email,
            $data[0]->remember_token,
            $data[0]->email_verified_at,
            $data[0]->alamat_siswa,
            $data[0]->alamat_siswa,
            $data[0]->created_at,
            $data[0]->updated_at,
            $data[0]->nomor_identitas,
            
        
        );


        return response()->json($data, 200);
    }
}
