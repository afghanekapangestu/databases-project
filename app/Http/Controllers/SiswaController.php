<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Nilai;

class SiswaController extends Controller
{
    public function getNilaiSiswa($nis){
        $data = Nilai::where('nis',$nis)->get();

        if($nis != auth()->user()->nomor_identitas){
            return redirect()->back()->with('status','Kamu tidak bisa melihat nilai murid lain');
        }

        return view('murid_layouts.lihat_nilai',compact('data'));
    }
}
