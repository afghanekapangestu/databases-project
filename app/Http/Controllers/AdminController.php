<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Guru;
use App\Model\Kelas;
use App\Model\Murid;

class AdminController extends Controller
{
    public function CreateUser(Request $r)
    {
        User::create([
            'name' => $r->name,
            'email' => $r->email,
            'role' => $r->role,
            'password' => bcrypt($r->password),
            'nomor_identitas' => $r->nomor_identitas,
            'remember_token' => \Str::random(60)
        ]);

        if ($r->role == 'guru') {

            $value = $_POST['mapel'];

            $val = implode(',', $value);

            Guru::create([
                'nip' => $r->nomor_identitas,
                'alamat_guru' => $r->alamat,
                'jk' => $r->jenis_kelamin,
                'mapel' => $val,
                'kelas' => $r->kelasMengajar
            ]);
        } else if ($r->role == 'siswa') {
            Murid::create([
                'nis' => $r->nomor_identitas,
                'alamat_siswa' => $r->alamat,
                'jk_siswa' => $r->jenis_kelamin,
                'id_kelas' => $r->kelas
            ]);
        }


        return redirect()->back()->with('status', 'Berhasil menambah data');
    }
    public function EditUser(Request $r, $id)
    {


        $data1 = User::where('id', $r->id)->update([
            'name' => $r->name,
            'email' => $r->email,
            'role' => $r->role,
            'nomor_identitas' => $r->nomor_identitas,
            'password' => bcrypt($r->password)
        ]);



        if ($data1) {
            if (User::where('id', $r->id)->first()) {
                $data = Guru::where('nip', '=', $r->nomor_identitas);
                $data->delete();

                $data1 = Murid::where('nis', '=', $r->nomor_identitas);
                $data1->delete();

                if ($r->role == "guru") { // Jika update ke status guru maka data di siswa dihapus
                    $data = Murid::where('nis', '=', $r->nomor_identitas);
                    $data->delete();

                    $value = $_POST['mapel'];

                    $val = implode(',', $value);

                    //Buat data baru di Guru
                    Guru::create([
                        'nip' => $r->nomor_identitas,
                        'alamat_guru' => $r->alamat,
                        'jk' => $r->jenis_kelamin,
                        'mapel' => $val,
                        'kelas' => $r->editkelasMengajar
                    ]);
                } else if ($r->role == "siswa") { //Jika update ke status siswa maka data di guru dihapus
                    $data = Guru::where('nip', '=', $r->nomor_identitas);
                    $data->delete();

                    // Buat data baru di siswa/murid
                    Murid::create([
                        'nis' => $r->nomor_identitas,
                        'alamat_siswa' => $r->alamat,
                        'jk_siswa' => $r->jenis_kelamin,
                        'id_kelas' => $r->kelas
                    ]);
                } else {
                    $data = Guru::where('nip', '=', $r->nomor_identitas);
                    $data->delete();

                    $data1 = Murid::where('nis', '=', $r->nomor_identitas);
                    $data1->delete();
                }
            }
        }

        return redirect()->back()->with('status', 'Berhasil edit Data');
    }



    public function DeleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('status', 'Berhasil menghapus data');
    }

    public function GetDataUser($role, $nomor_identitas)
    {

        if ($role == "siswa") {
            $user = \DB::table('users')
                ->join('siswa', 'siswa.nis', '=', 'users.nomor_identitas')
                ->where('nis', $nomor_identitas)
                ->get();
        } else if ($role == "guru") {
            $user = \DB::table('users')
                ->join('guru', 'guru.nip', '=', 'users.nomor_identitas')
                ->where('nip', $nomor_identitas)
                ->get();
        } else if ($role == "admin") {
            $user = \DB::table('users')
                ->where('nomor_identitas', $nomor_identitas)
                ->get();
        }

        return response()->json($user, 200);
    }

    public function editKelas(Request $r)
    {
        Kelas::where('id_kelas', $r->id_kelas)->update([
            'id_prodi' => $r->id_prodi,
            'id_kelasgrup' => $r->id_kelasgrup,
            'id_nomor' => $r->nomor
        ]);

        return redirect()->back();
    }

    public function deletekelas($id)
    {
        $data = Kelas::find($id);
        $data->delete();

        return redirect()->back()->with('status', 'Berhasi menghapus kelas!');
    }

    public function createkelas(Request $r)
    {
        Kelas::create([
            'id_kelasgrup' => $r->id_kelasgrup,
            'id_prodi' => $r->id_prodi,
            'id_nomor' => $r->nomor
        ]);

        return redirect()->back()->with('status', 'Kelas Berhasil ditambahkan!');
    }

    public function dashboard()
    {
        $admin = \DB::table('users')->where('role', 'admin')->count();
        $guru = \DB::table('users')->where('role', 'guru')->count();
        $siswa = \DB::table('users')->where('role', 'siswa')->count();
        $all = \DB::table('users')->count();
        return view('admin_layouts.dashboard_admin', compact('admin', 'guru', 'siswa', 'all'));
    }
}
