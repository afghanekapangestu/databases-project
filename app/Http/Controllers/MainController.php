<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    // Fetch data

    public function getData(){
        $data = \DB::table('users')->get();
        $mapel = \DB::table('mapel')->get();
        $kelas = \DB::table('kelas')
                ->join('prodi','kelas.id_prodi','=','prodi.id_prodi')
                ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
                ->join('nomor_kelas','kelas.id_nomor','=','nomor_kelas.id_nomor')
                ->orderBy('id_kelas','ASC')
                ->get();

        return view('admin_layouts.tambah_data',compact('data','mapel','kelas'));
    }

    public function getDataKelas(){
        $data = \DB::table('kelas')
                ->join('prodi','kelas.id_prodi','=','prodi.id_prodi')
                ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
                ->join('nomor_kelas','kelas.id_nomor','=','nomor_kelas.id_nomor')
                ->get();

        $data1 = \DB::table('prodi')->get();
        $data2 = \DB::table('kelas_grup')->get();
        $data3 = \DB::table('nomor_kelas')->get();
                
        return view('admin_layouts.kelas',compact('data','data1','data2','data3'));
    }

    public function getDataKelasEdit($id_kelas){
        $data = \DB::table('kelas')
                ->join('prodi','kelas.id_prodi','=','prodi.id_prodi')
                ->join('kelas_grup','kelas.id_kelasgrup','=','kelas_grup.id_kelasgrup')
                ->join('nomor_kelas','kelas.id_nomor','=','nomor_kelas.id_nomor')
                ->join('nis','siswa.id_kelas','=','kelas.id_kelas')
                ->where('id_kelas',$id_kelas)
                ->get();

        return response()->json($data, 200);
    }

}
