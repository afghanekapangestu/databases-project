<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    // Form Login
    public function formLogin(){
        return view('login');
    }

    // Login Process
    public function prosesLogin(Request $request){
        if(Auth::attempt($request->only('nomor_identitas','password'))){
            if(auth()->user()->role == "admin"){
                return redirect('/dashboard_admin');
            }else if(auth()->user()->role == "siswa"){
                return redirect('/dashboard_siswa/'.$request->nomor_identitas);
            }else if(auth()->user()->role == "guru"){
                return redirect('/dashboard_guru');
            }
        }

        return redirect('/')->with('status','Gagal Login');
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
